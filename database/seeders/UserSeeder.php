<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $users = User::factory(10)->create();

        $role = Role::create(['name'=>'customer']);

        $all_permissions = [Permission::create(['name'=>'category.view']),Permission::create(['name'=>'product.view'])];

        $role->syncPermissions($all_permissions);

        foreach ($users as $user)
            $user->assignRole('customer');

        $role = Role::create(['name'=>'admin']);

        $admin_permissions = [Permission::create(['name'=>'category.create']),Permission::create(['name'=>'product.create']), Permission::create(['name'=>'category.edit']),Permission::create(['name'=>'product.edit']), Permission::create(['name'=>'category.delete']),Permission::create(['name'=>'product.delete'])];


        $role->syncPermissions($admin_permissions);

        $user = User::create(['name'=>'admin',
        'email'=>'admin@admin.com',
        'password'=>Hash::make('qwerty123'),
        'remember_token' => Str::random(10),
        'email_verified_at' => now()]);

        $user->assignRole('customer','admin');
    }
}
