<?php

return [

   'no_products'=>'No products yet. Please check back later.',
   'no_categories'=>'No categories yet. Please check back later.',
   'products'=>'Products',
   'categories'=>'Categories',
   'admin_products'=>'Admin Products',
   'admin_categories'=>'Admin Categories',
   'login'=>'Login',
   'view_product'=>'View Product',
   'category'=>' Category',
   'back'=>' Back',
   'view_category'=>' View Category',
   'view'=>' View',
   'create_pr'=>' Create Product',
   'edit'=>'Edit',
   'edit_pr'=>'Edit Product',
   'name'=>'Name',
   'img'=>'Image',
   'description'=>'Description',
   'save'=>'Save',
   'create_cat'=>'Create Category',
   'id'=>'Id',
   'delete'=>'Delete',


];