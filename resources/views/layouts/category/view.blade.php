<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{ __('app.view_category') }}
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    {{$category->name}}
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <img src="{{ asset('storage/' . $category->img) }}" />
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    {{$category->description}}
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{route('product')}}" class="transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8"> < {{ __('app.back') }} </a>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>