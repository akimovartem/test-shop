<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
    <div class="space-y-2 lg:space-y-0 lg:space-x-4 mt-4">


        <!-- Search -->
        {{--<div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">--}}
            {{--<form method="GET" action="/">--}}
                {{--@if (request('category'))--}}
                    {{--<input type="hidden" name="category" value="{{ request('category') }}">--}}
                {{--@endif--}}

                {{--<input type="text"--}}
                       {{--name="search"--}}
                       {{--placeholder="Find something"--}}
                       {{--class="bg-transparent placeholder-black font-semibold text-sm"--}}
                       {{--value="{{ request('search') }}"--}}
                {{-->--}}
            {{--</form>--}}
        {{--</div>--}}
    </div>
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
    </body>
</html>
