<x-app-layout>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($categories->count())
                        <x-categories-grid :categories="$categories" />

                        {{ $categories->links() }}
                    @else
                        <p class="text-center">{{__('app.no_categories')}}</p>
                    @endif
                </div>


            </div>
        </div>
    </div>
</x-app-layout>