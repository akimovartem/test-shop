@props(['categories'])

@if ($categories->count() > 1)
    <div class="lg:grid lg:grid-cols-6">
        @foreach ($categories as $category)
            <x-category-card
                    :category="$category"
                    class="col-span-3"
            />
        @endforeach
    </div>

@endif