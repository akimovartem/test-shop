@props(['product'])

<article
        {{ $attributes->merge(['class' => 'transition-colors duration-300 hover:bg-gray-100 border border-black border-opacity-0 hover:border-opacity-5 rounded-xl']) }}>
    <div class="py-6 px-5 h-full flex flex-col">

        <div class="mt-6 flex flex-col justify-between flex-1">
            <header>

                <div class="mt-4">
                    <h1 class="text-3xl clamp one-line">
                        <a href="/product/{{ $product->id }}">
                            {{ $product->name }}
                        </a>
                    </h1>
                </div>
            </header>

            <body>
            <img src="{!! asset('storage/' . $product->img) !!} " alt="{{asset($product->img)}}" width="200">
            <div class="flex items-center text-sm">
                <div class="ml-3">
                    <h5 class="font-bold">
                       {{$product->description}}
                    </h5>
                </div>
            </div>
            </body>

            <footer class="flex justify-between items-center mt-8">
                <div class="flex items-center text-sm">
                    <div class="ml-3">
                        <h5 class="font-bold">
                            {{ __('app.category') }} :  {{$product->category->name??""}}
                        </h5>
                    </div>
                </div>
            @can('product.view')
                <div>
                    <a href="/product/{{ $product->id }}"
                       class="transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8"
                    >{{ __('app.view') }}</a>
                </div>
            @endcan
            </footer>
        </div>
    </div>
</article>