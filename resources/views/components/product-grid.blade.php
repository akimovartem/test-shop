@props(['products'])

{{--<x-post-featured-card :post="$posts[0]" />--}}

@if ($products->count() > 1)
    <div class="lg:grid lg:grid-cols-6">
        @foreach ($products as $product)
            <x-product-card
                    :product="$product"
                    class="col-span-3"
            />
        @endforeach
    </div>

@endif