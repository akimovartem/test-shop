<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                            <a href="{{route('product.create')}}" class="text-indigo-600 hover:text-indigo-900">{{ __('app.create_pr') }}</a>
                        </td>
                        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                <tr>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('app.id') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('app.name') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('app.category') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('app.description') }}
                                    </th>
                                    <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        {{ __('app.img') }}
                                    </th>
                                    <th scope="col" class="relative px-6 py-3">
                                    </th>
                                    <th scope="col" class="relative px-6 py-3">
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="bg-white divide-y divide-gray-200">
                                @foreach($products as $product)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {!! $product->id !!}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {!! substr($product->name,0,30) !!}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {!! $product->category->name??"" !!}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {!! substr($product->description,0,40) !!}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        <img src="{{ asset('storage/' . $product->img) }}" alt="not found image" width="100"/>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                        <a href="{{route('product.edit',$product->id)}}" class="text-indigo-600 hover:text-indigo-900">{{ __('app.edit') }}</a>
                                    </td>
                                    {{ Form::open((['url' => route('product.destroy',$product->id),'method'=>'DELETE'])) }}

                                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                            {{Form::submit( __('app.delete') ,['class'=>'text-indigo-600 hover:text-indigo-900'])}}
                                        </td>

                                    {{ Form::close() }}
                                </tr>
                                @endforeach
                                <!-- More people... -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {!! $products->links() !!}
                </div>
            </div>
        </div>
    </div>




</x-app-layout>
