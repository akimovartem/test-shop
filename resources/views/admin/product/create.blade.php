<x-app-layout>

    <style>
        input{
            width: 100%;
        }
    </style>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{__('app.create_pr')}}
                </div>
                {!! Form::open(['url' => route('product.store'),'method'=>'POST','files'=>true]) !!}
                <div class="p-6 bg-white border-b border-gray-200">
                    {!! Form::label('name', __('app.name')) !!}
                    {!! Form::text('name',old('name'),['class'=>'']) !!}
                    <x-error name="name"/>

                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    {!! Form::label('category_id', __('app.category')) !!}
                    {!! Form::select('category_id', $categories) !!}
                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex-1">
                        {!! Form::label('ing', __('app.img')) !!}
                        {!! Form::file('img',['value'=> old('img')])  !!}
                        <x-error name="img"/>

                    </div>
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    {!! Form::label('description', __('app.description')) !!}
                    {!! Form::text('description',old('description')) !!}
                    <x-error name="description"/>

                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    {!! Form::submit(__('app.save'),['class'=>'transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8']) !!}
                </div>

                {!! Form::close() !!}
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{route('admin.products')}}" class="transition-colors duration-300 text-xs font-semibold bg-gray-200 hover:bg-gray-300 rounded-full py-2 px-8"> < {{__('app.back')}} </a>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>