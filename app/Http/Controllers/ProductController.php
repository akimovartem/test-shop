<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(10);

        return view('dashboard')->withProducts($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.product.create')->withCategories($this->get_categories());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $request = $request->all();
        if ($request['img'] ?? false) {

            $request['img'] =  $request['img']->store('thumbnails');
        }

        Product::create($request);


        return redirect(route('admin.products'))->with('success', 'Your category has been created.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('layouts.product.view')->withProduct($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {



        return view('admin.product.edit')->withProduct($product)->withCategories($this->get_categories());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {

       $request = $request->all();


       foreach ($request as $key => $value){
           if(isset($product->$key)){
               $product->$key = $value;
           }
       }


        if ($request['img'] ?? false) {

            $product['img'] =  $request['img']->store('thumbnails');
        }

        $product->save();


        return redirect(route('admin.products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {

        $product->delete();

        return redirect(route('admin.products'));
    }

    public function get_categories(){
        $categories=Category::select('id','name')->get()->keyBy('id')->toArray();

        foreach ($categories as $key => $category){
            $categories[$key]=$category['name'];
        }
        return $categories;
    }
}
