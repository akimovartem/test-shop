<?php

namespace App\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Str;
use Auth;

class GoogleController extends Controller
{
    public function redirectToGoogle()

    {

        return Socialite::driver('google')->redirect();

    }



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function handleGoogleCallback()

    {

        try {



            $user = Socialite::driver('google')->user();
            $finduser = User::where('google_id', $user->id)->first();

            if($finduser){

                Auth::login($finduser);
                return redirect(route('product'));

            }else{

                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'password' => encrypt('qwerty123'),
                    'email_verified_at' => now(),
                    'remember_token' => Str::random(10),
                ]);

                $newUser->assignRole('customer');

                Auth::login($newUser);
                return redirect(route('product'));

            }



        } catch (Exception $exception) {

            dd($exception->getMessage());

        }

    }
}
