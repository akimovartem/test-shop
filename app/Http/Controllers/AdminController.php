<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function products(){

        return view('admin.product.list')->withProducts(Product::paginate(25));

    }

    public function categories(){
        return view('admin.category.list')->withCategories(Category::paginate(25));
    }
}
