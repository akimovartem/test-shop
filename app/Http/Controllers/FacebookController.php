<?php

namespace App\Http\Controllers;

use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Str;
use Auth;

class FacebookController extends Controller
{
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function loginWithFacebook()
    {
        try {

            $user = Socialite::driver('facebook')->user();
            $isUser = User::where('fb_id', $user->id)->first();

            if($isUser){
                Auth::login($isUser);
                return redirect(route('product'));
            }else{
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'fb_id' => $user->id,
                    'password' => encrypt('qwerty123'),
                    'email_verified_at' => now(),
                    'remember_token' => Str::random(10),
                ]);

                $createUser->assignRole('customer');

                Auth::login($createUser);
                return redirect(route('product'));
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
