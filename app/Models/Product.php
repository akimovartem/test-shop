<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Log;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [
        '_method',
        '_token'
    ];

    protected $dates = ['deleted_at'];


    public function category(){

        return $this->belongsTo(Category::class);

    }

    public static function boot(){
        parent::boot();


        static::creating(function($item) {

            Log::info('Creating product call: '.$item);
        });


        static::created(function($item) {

            Log::info('Created product call: '.$item);

        });

        static::updating(function($item) {

            Log::info('Updating product call: '.$item);

        });

        static::updated(function($item) {

            Log::info('Updated product call: '.$item);

        });

        static::deleted(function($item) {

            Log::info('Deleted product call: '.$item);

        });


    }

}
