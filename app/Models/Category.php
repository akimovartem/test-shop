<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Log;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [
        '_method',
        '_token'
    ];

    public function products(){

        return $this->hasMany(Product::class);

    }

    public static function boot(){
        parent::boot();


        static::creating(function($item) {

            Log::info('Creating category call: '.$item);
        });


        static::created(function($item) {

            Log::info('Created category call: '.$item);

        });

        static::updating(function($item) {

            Log::info('Updating category call: '.$item);

        });

        static::updated(function($item) {

            Log::info('Updated category call: '.$item);

        });

        static::deleted(function($item) {

            Log::info('Deleted category call: '.$item);

        });


    }

}
