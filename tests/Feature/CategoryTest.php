<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Log;


class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    public function test_category_index()
    {
        $response = $this->get('/category');
        $response->assertStatus(200);
    }

    public function test_a_admin_view_can_be_forbidden()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('admin/categories');
        $response->assertForbidden();
    }

    public function test_category_admin(){
        $user= User::find(11);
        $response =$this->actingAs($user)->get('admin/categories');
        $response->assertStatus(200);
    }

    public function test_an_action_that_requires_authentication()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->get('/');
        $response->assertStatus(200);
    }

    public function test_a_index_view_can_be_find_user_name()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('/');
        $response->assertSee( $user->name);
    }

    public function test_a_add_categoryes()
    {
        $categories_count = Category::all()->count();
        Category::factory(5)->create();
        $this->assertDatabaseCount('categories', $categories_count+5);
    }

    public function test_a_delete_categoryes()
    {
        $category = Category::find(1);
        $category->delete();
        $this->assertDeleted($category);
    }


}
