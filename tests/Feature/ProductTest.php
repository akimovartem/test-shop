<?php

namespace Tests\Feature;

use App\Models\Product;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    public function test_product_index()
    {
        $response = $this->get('/product');
        $response->assertStatus(200);
    }

    public function test_a_admin_view_can_be_forbidden()
    {
        $user = User::factory()->create();
        $response = $this->actingAs($user)->get('admin/products');
        $response->assertForbidden();
    }

    public function test_category_admin(){
        $user= User::find(11);
        $response =$this->actingAs($user)->get('admin/products');
        $response->assertStatus(200);
    }

    public function test_a_add_products()
    {
        $product_count = Product::all()->count();
        Product::factory(5)->create();
        $this->assertDatabaseCount('products', $product_count+5);
    }

}
