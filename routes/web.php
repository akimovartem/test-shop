<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Models\User;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\FacebookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[ProductController::class,'index'])->name('product');

Route::group(['middleware' => 'role:admin'], function() {
    Route::get('/admin/products',[AdminController::class,'products'])->middleware(['auth'])->name('admin.products');
    Route::get('/admin/categories',[AdminController::class,'categories'])->middleware(['auth'])->name('admin.categories');
    Route::resource('category',CategoryController::class )->except(['index','show']);
    Route::resource('product', ProductController::class)->except(['index','show']);

});


Route::resource('category',CategoryController::class )->only(['index','show']);
Route::resource('product', ProductController::class)->only(['index','show']);

Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('/gl/callback', [GoogleController::class, 'handleGoogleCallback']);

Route::get('auth/facebook', [FacebookController::class, 'facebookRedirect']);
Route::get('/fb/callback', [FacebookController::class, 'loginWithFacebook']);

Route::get('lang/{locale?}', function ($locale = null) {
    app()->setLocale($locale);
    session()->put('locale', $locale);
    return redirect()->back();
});

require __DIR__.'/auth.php';
